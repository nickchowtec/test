﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Acme.Tests.Helpers;
using AcmeMessageProcessorApplication.Models;
using NUnit.Framework;
using Shouldly;

namespace Acme.Tests.UnitTests
{
    [TestFixture]
    public class BabyBirthMessageTest : AcmeTestBase
    {
        [Test]
        public void CanCreateBabyBirthMessage()
        {
            var message = Mock.NewBabyBirthMessage("recipient");
            var path = Mock.GetBabyBirthFolderPath();
            var expectedResult = MessageProceedResult.CreateResult(Path.Combine(path, String.Concat(message.MessageId.ToString(), ".xml")), MessageProceedStatus.Success);

            MessageStore.SendMessage(message);
            var proceedMessage = MessageStore.ProceedQueue().FirstOrDefault();
            proceedMessage.ShouldNotBe(null);
            var proceedResult = proceedMessage.ProceedMessage();

            proceedResult.Status.ShouldBe(expectedResult.Status);
            proceedResult.FilePath.ShouldBe(expectedResult.FilePath);
            proceedResult.FailureMessage.ShouldBe(expectedResult.FailureMessage);
            File.Exists(proceedResult.FilePath).ShouldBe(true);
        }
        
        [Test]
        public void WhenCreateBabyBirthMessage_WithName_ShouldBeEncodedBase64()
        {
            var message = Mock.NewBabyBirthMessage("recipient123");

            MessageStore.SendMessage(message);
            var proceedMessage = MessageStore.ProceedQueue().FirstOrDefault();
            proceedMessage.ShouldNotBe(null);
            proceedMessage.ProceedMessage();

            byte[] data = Convert.FromBase64String(proceedMessage.Name);
            string decodedMessageName = Encoding.UTF8.GetString(data);

            decodedMessageName.ShouldBe("recipient123");
        }

        [Test]
        public void WhenCreateBabyBirthMessage_WithBabyBirthday_ShouldBeFormated()
        {
            var message = Mock.NewBabyBirthMessage("recipient");
            MessageStore.SendMessage(message);
            var proceedMessage = MessageStore.ProceedQueue().FirstOrDefault();
            proceedMessage.ShouldNotBe(null);
            proceedMessage.ProceedMessage();
            var msg = (BabyBirthMessage)proceedMessage;
            var today = DateTime.Today;
            msg.FormattedBabyBirthDay.ShouldBe(today.ToString("dd MMM yyyy", new CultureInfo("en-GB")));
        }

        [Test]
        public void WhenCreateBabyBirthMessage_WithFileFormat_ShouldBeXml()
        {
            var message = Mock.NewBabyBirthMessage("recipient");

            MessageStore.SendMessage(message);
            var proceedMessage = MessageStore.ProceedQueue().FirstOrDefault();
            proceedMessage.ShouldNotBe(null);
            var proceedResult = proceedMessage.ProceedMessage();
            var extension = Path.GetExtension(proceedResult.FilePath);

            extension.ShouldBe(".xml");
        }
    }
}