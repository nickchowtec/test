﻿using System;
using System.IO;
using System.Linq;
using Acme.Tests.Helpers;
using AcmeMessageProcessorApplication.Models;
using NUnit.Framework;
using Shouldly;

namespace Acme.Tests.UnitTests
{
    [TestFixture]
    public class BirthDayMessageProcessTest : AcmeTestBase
    {
        [Test]
        public void CanCreateBirthDayMessage()
        {
            var message = Mock.NewBirthdayMessage("recipient");
            var path = Mock.GetBirthdayFolderPath();
            var expectedResult = MessageProceedResult.CreateResult(Path.Combine(path, String.Concat(message.MessageId.ToString(), ".json")), MessageProceedStatus.Success);

            MessageStore.SendMessage(message);
            var proceedMessage = MessageStore.ProceedQueue().FirstOrDefault();
            proceedMessage.ShouldNotBe(null);
            var proceedResult = proceedMessage.ProceedMessage();

            proceedResult.Status.ShouldBe(expectedResult.Status);
            proceedResult.FilePath.ShouldBe(expectedResult.FilePath);
            proceedResult.FailureMessage.ShouldBe(expectedResult.FailureMessage);
            File.Exists(proceedResult.FilePath).ShouldBe(true);
        }

        [Test]
        public void WhenCreateBirthDayMessage_WithMessageText_ShouldBeInUpperCase()
        {
            var message = Mock.NewBirthdayMessage("recipient","It should all in upper case.");

            MessageStore.SendMessage(message);
            var proceedMessage = MessageStore.ProceedQueue().FirstOrDefault();
            proceedMessage.ShouldNotBe(null);
            proceedMessage.ProceedMessage();

            string.IsNullOrEmpty(proceedMessage.MessageText).ShouldBe(false);
            proceedMessage.MessageText.Any(char.IsLower).ShouldBe(false);
        }


        [Test]
        public void WhenCreateBirthDayMessage_WithFileFormat_ShouldBeJson()
        {
            var message = Mock.NewBirthdayMessage("recipient");
            MessageStore.SendMessage(message);
            var proceedMessage = MessageStore.ProceedQueue().FirstOrDefault();
            proceedMessage.ShouldNotBe(null);
            var proceedResult = proceedMessage.ProceedMessage();
            var extension = Path.GetExtension(proceedResult.FilePath);

            extension.ShouldBe(".json");
        }
    }
}
