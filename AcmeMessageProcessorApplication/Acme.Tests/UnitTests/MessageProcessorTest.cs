﻿using System;
using System.Collections.Generic;
using System.IO;
using Acme.Tests.Helpers;
using AcmeMessageProcessorApplication.Models;
using NUnit.Framework;
using Shouldly;

namespace Acme.Tests.UnitTests
{
    [TestFixture]
    public class ProcessorTest : AcmeTestBase
    {
        [Test]
        public void TestProcessorProceedQueueMethod()
        {
            var processor = new MessageStore.MessageStore();
            var birthMessage = Mock.NewBirthdayMessage();
            var babyBirthMessage = Mock.NewBabyBirthMessage();
            var logPath = Mock.GetLogFolderPath();

            processor.SendMessage(babyBirthMessage);
            processor.SendMessage(birthMessage);

            processor.ProceedQueue();

            File.Exists(Path.Combine(logPath, string.Format("{0}.log", DateTime.Today.ToString("yyyy-MM-dd")))).ShouldBe(true);
        }
        
        private IEnumerable<IMessage> GetSampleMessageArray()
        {
            return new IMessage[] 
            {
                Mock.NewBabyBirthMessage(),
                Mock.NewBirthdayMessage()
            };
        }
    }
}