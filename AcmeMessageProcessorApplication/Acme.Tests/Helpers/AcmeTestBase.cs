﻿namespace Acme.Tests.Helpers
{
    public abstract class AcmeTestBase
    {
        protected readonly MockEntities Mock = new MockEntities();
        protected static MessageStore.MessageStore MessageStore = new MessageStore.MessageStore();
    }
}
