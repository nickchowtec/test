﻿using System;
using System.Configuration;
using AcmeMessageProcessorApplication.Models;


namespace Acme.Tests.Helpers
{
    public class MockEntities
    {
        private static readonly string RootFolderPath = ConfigurationManager.AppSettings["RootFolderPath"];

        public string GetBirthdayFolderPath()
        {
            return RootFolderPath + ConfigurationManager.AppSettings["BirthdayFolderPath"];
        }

        public string GetBabyBirthFolderPath()
        {
            return RootFolderPath + ConfigurationManager.AppSettings["BabyBirthFolderPath"];
        }

        public string GetLogFolderPath()
        {
            return RootFolderPath + ConfigurationManager.AppSettings["LogFolderPath"];
        }

        public BabyBirthMessage NewBabyBirthMessage(String name = null, String text = null, Gender gender = Gender.Male, DateTime? bDay=null)
        {
            return new BabyBirthMessage()
                   {
                       MessageId = Guid.NewGuid(),
                       Name = name,
                       MessageText =text?? "text",
                       Gender = gender,
                       BabyBirthDay = bDay ?? DateTime.Today,
                       OutputFormat = MessageSerializeFormat.Xml,
                       MessageType = MessageType.BabyBirth
                   };
        }

        public BirthdayMessage NewBirthdayMessage(String name = null, String text = null, String gift = null, DateTime? bDay = null)
        {
            return new BirthdayMessage()
            {
                MessageId = Guid.NewGuid(),
                Name = name,
                MessageText = text ?? "text",
                Gift = gift??"car",
                BirthDate = bDay ?? DateTime.Today,
                OutputFormat = MessageSerializeFormat.Json,
                MessageType = MessageType.Birthday
            };
        }
    }
}
