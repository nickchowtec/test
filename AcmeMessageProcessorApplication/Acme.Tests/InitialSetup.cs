﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;
using Acme.Tests.Helpers;
using NUnit.Framework;

namespace Acme.Tests
{
    [SetUpFixture]
    public class InitialSetup
    {
        private const string Queue = @".\Private$\acme";

        [SetUp]
        public void RunBeforeAnyTests()
        {
            var mock = new MockEntities();
            var logPath = mock.GetLogFolderPath();
            var babyBirthFolderPath = mock.GetBabyBirthFolderPath();
            var birthdayFolderPath = mock.GetBirthdayFolderPath();
            CreateDirectoryIfNotExists(logPath);
            CreateDirectoryIfNotExists(babyBirthFolderPath);
            CreateDirectoryIfNotExists(birthdayFolderPath);
            CreateQueue(Queue);
        }

        [TearDown]
        public void RunAfterAnyTests()
        {
        }

        private static void CreateDirectoryIfNotExists(string path)
        {
            if (Directory.Exists(path)) return;
            var dir = new DirectoryInfo(path);
            dir.Create();
        }

        public void CreateQueue(string qname)
        {
            if (!MessageQueue.Exists(qname)) MessageQueue.Create(qname);
        }
    }
}
