﻿using System;
using System.IO;
using System.Xml.Serialization;
using AcmeMessageProcessorApplication.Models;
using Newtonsoft.Json;

namespace AcmeMessageProcessorApplication
{
    /// <summary>
    /// serializer class
    /// </summary>
    internal class Serializer
    {
        /// <summary>
        /// serialize message
        /// </summary>
        /// <param name="message">message</param>
        /// <param name="outputFile">output filename</param>
        /// <returns>result</returns>
        public static MessageProceedResult SerializeMessage(IMessage message, string outputFile)
        {
            switch (message.OutputFormat)
            {
                case MessageSerializeFormat.Json:
                    return SerializeToJson(message, outputFile);
                case MessageSerializeFormat.Xml:
                    return SerializeToXml(message, outputFile);
                default:
                    return MessageProceedResult.CreateResult(null, MessageProceedStatus.Failed);
            }
        }

        /// <summary>
        /// serialize to json
        /// </summary>
        /// <param name="message">message</param>
        /// <param name="outputFile">output filename</param>
        /// <returns>result</returns>
        private static MessageProceedResult SerializeToJson(IMessage message, string outputFile)
        {
            outputFile += ".json";

            try
            {
                var json = JsonConvert.SerializeObject(message);

                File.WriteAllText(outputFile, json);

                return MessageProceedResult.CreateResult(outputFile, MessageProceedStatus.Success);
            }
            catch (Exception e)
            {
                return MessageProceedResult.CreateResult("",MessageProceedStatus.Failed,e.Message);
            }
        }

        /// <summary>
        /// serialize to xml
        /// </summary>
        /// <param name="message">message</param>
        /// <param name="outputFile">output filename</param>
        /// <returns></returns>
        private static MessageProceedResult SerializeToXml(IMessage message, string outputFile)
        {
            outputFile += ".xml";

            try
            {
                var xmlSerializer = new XmlSerializer(message.GetType());

                using (var file = new StreamWriter(outputFile))
                {
                    xmlSerializer.Serialize(file, message);

                    file.Flush();
                    file.Close();
                }

                return MessageProceedResult.CreateResult(outputFile,MessageProceedStatus.Success);
            }
            catch (Exception e)
            {
                return MessageProceedResult.CreateResult(null,MessageProceedStatus.Failed,e.Message);
            }
        }
    }
}