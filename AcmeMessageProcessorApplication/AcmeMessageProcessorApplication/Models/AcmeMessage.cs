﻿using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AcmeMessageProcessorApplication.Models
{
    /// <summary>
    /// base message fields implementation
    /// </summary>
    public abstract class AcmeMessage : IMessage
    {
        /// <summary>
        /// get or set message id
        /// </summary>
        public Guid MessageId { get; set; }

        /// <summary>
        /// get or set message type
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public MessageType MessageType { get; set; }

        /// <summary>
        /// get or set recipient name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// get or set message text
        /// </summary>
        public string MessageText { get; set; }

        /// <summary>
        /// get or set output format
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public MessageSerializeFormat OutputFormat { get; set; }

        /// <summary>
        /// base constructor
        /// </summary>
        protected AcmeMessage() { }

        /// <summary>
        /// values constructor
        /// </summary>
        /// <param name="messageId">message id</param>
        /// <param name="messageType">message type</param>
        /// <param name="name">name</param>
        /// <param name="messageText">message text</param>
        /// <param name="outputFormat">output Format</param>
        protected AcmeMessage(Guid messageId, MessageType messageType, string name, string messageText, MessageSerializeFormat outputFormat)
        {
            this.MessageId = messageId;
            this.MessageType = messageType;
            this.Name = name;
            this.MessageText = messageText;
            this.OutputFormat = outputFormat;
        }

        /// <summary>
        /// proceed message method
        /// </summary>
        /// <returns>result</returns>
        public abstract MessageProceedResult ProceedMessage();

        /// <summary>
        /// get file name
        /// </summary>
        /// <param name="folder">folder</param>
        /// <returns>file name</returns>
        protected string GetFileNameForSerializer(string folder)
        {
            // set it to {folder}\{MessageId} without extension
            return Path.Combine(folder, this.MessageId.ToString());
        }
    }

}
