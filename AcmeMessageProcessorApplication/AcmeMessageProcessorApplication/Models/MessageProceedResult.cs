﻿
namespace AcmeMessageProcessorApplication.Models
{
    /// <summary>
    /// message proceed result
    /// </summary>
    public class MessageProceedResult
    {
        /// <summary>
        /// get or set status
        /// </summary>
        public MessageProceedStatus Status { get; set; }

        /// <summary>
        /// get or set failure message
        /// </summary>
        public string FailureMessage { get; set; }

        /// <summary>
        /// get or set success file path
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// create failed result
        /// </summary>
        /// <param name="status"></param>
        /// <param name="message">failed message</param>
        /// <param name="filePath"></param>
        /// <returns>failed result</returns>
        public static MessageProceedResult CreateResult(string filePath, MessageProceedStatus status, string message="")
        {
            return new MessageProceedResult
            {
                Status = status,
                FilePath = filePath,
                FailureMessage = message
            };
        }
    }
}