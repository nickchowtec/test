﻿using System;

namespace AcmeMessageProcessorApplication.Models
{
    public interface IMessage
    {
        /// <summary>
        /// get or set message id
        /// </summary>
        Guid MessageId { get; set; }

        /// <summary>
        /// get or set message type
        /// </summary>
        MessageType MessageType { get; set; }

        /// <summary>
        /// get or set recipient name
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// get or set message text
        /// </summary>
        string MessageText { get; set; }

        /// <summary>
        /// get or set output format
        /// </summary>
        MessageSerializeFormat OutputFormat { get; set; }

        /// <summary>
        /// proceed message method
        /// </summary>
        /// <returns></returns>
        MessageProceedResult ProceedMessage();
    }

    public enum MessageSerializeFormat
    {
        /// <summary>
        /// no serialization
        /// </summary>
        None,
        /// <summary>
        /// json serialization
        /// </summary>
        Json,
        /// <summary>
        /// xml serialization
        /// </summary>
        Xml
    }
}
