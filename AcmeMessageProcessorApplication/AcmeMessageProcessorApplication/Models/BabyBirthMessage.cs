﻿using System;
using System.Globalization;

namespace AcmeMessageProcessorApplication.Models
{
    /// <summary>
    /// child birth message
    /// </summary>
    public class BabyBirthMessage : AcmeMessage
    {
        /// <summary>
        /// child gender
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// baby birthday
        /// </summary>
        public DateTime BabyBirthDay { get; set; }

        /// <summary>
        /// formatted baby birthday
        /// </summary>
        public string FormattedBabyBirthDay { get; set; }

        /// <summary>
        /// base constructor
        /// </summary>
        public BabyBirthMessage() { }

        /// <summary>
        /// base constructor
        /// </summary>
        public BabyBirthMessage(Guid messageId, string name, string messageText, Gender gender, DateTime babyBirthDay) :
            base(messageId, MessageType.BabyBirth, name, messageText, MessageSerializeFormat.Xml)
        {
            this.Gender = gender;
            this.BabyBirthDay = babyBirthDay;
        }

        /// <summary>
        /// proceed message
        /// </summary>
        /// <returns></returns>
        public override MessageProceedResult ProceedMessage()
        {
            // encode name without using encoding
            // we can use UTF8 encofing as well:
            // this.Name = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(this.Name))
            this.Name = Convert.ToBase64String(getBytes(this.Name));

            // format baby birthday
            this.FormattedBabyBirthDay = this.BabyBirthDay.ToString("dd MMM yyyy", new CultureInfo("en-GB"));

            // serialize
            return Serializer.SerializeMessage(this,this.GetFileNameForSerializer(ConfigReader.BabyBirthFolderPath));
        }

        /// <summary>
        /// get string bytes
        /// </summary>
        /// <param name="value">value</param>
        /// <returns>byte array</returns>
        private byte[] getBytes(string value)
        {
            return String.IsNullOrEmpty(value) ? new byte[0] : System.Text.Encoding.UTF8.GetBytes(value);
        }
    }
}