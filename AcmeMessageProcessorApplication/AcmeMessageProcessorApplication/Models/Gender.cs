﻿
namespace AcmeMessageProcessorApplication.Models
{
    /// <summary>
    /// child gender
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// male gender
        /// </summary>
        Male,
        /// <summary>
        /// female gender
        /// </summary>
        Female
    }
}