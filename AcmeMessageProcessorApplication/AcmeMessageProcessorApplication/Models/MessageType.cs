﻿
namespace AcmeMessageProcessorApplication.Models
{
    /// <summary>
    /// message type
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// default case
        /// </summary>
        Unknown,
        /// <summary>
        /// birthday message
        /// </summary>
        Birthday,
        /// <summary>
        /// baby birth message
        /// </summary>
        BabyBirth
    }
}