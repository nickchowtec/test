﻿using System.Configuration;

namespace AcmeMessageProcessorApplication
{
    /// <summary>
    /// application configuration reader
    /// </summary>
    internal class ConfigReader
    {
        /// <summary>
        /// get root folder
        /// </summary>
        public static readonly string RootFolder = ConfigurationManager.AppSettings["RootFolderPath"];

        /// <summary>
        /// get birthday folder
        /// </summary>
        public static readonly string BirthdayFolder = RootFolder + ConfigurationManager.AppSettings["BirthdayFolderPath"];

        /// <summary>
        /// get baby birth folder
        /// </summary>
        public static readonly string BabyBirthFolderPath = RootFolder + ConfigurationManager.AppSettings["BabyBirthFolderPath"];

        /// <summary>
        /// get log folder
        /// </summary>
        public static readonly string LogFolderPath = RootFolder + ConfigurationManager.AppSettings["LogFolderPath"];
    }
}