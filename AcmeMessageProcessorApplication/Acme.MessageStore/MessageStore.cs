﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Messaging;
using AcmeMessageProcessorApplication.Models;

namespace Acme.MessageStore
{
    /// <summary>
    ///     message processor
    /// </summary>
    public class MessageStore
    {
        /// <summary>
        ///     message queue
        /// </summary>
        //private List<IMessage> queue = new List<IMessage>();
        private MessageQueue queue = new MessageQueue(@".\Private$\acme");

        /// <summary>
        ///     add message in queue
        /// </summary>
        /// <param name="message"></param>
        public void SendMessage(IMessage message)
        {
            // add to queue
            queue.Send(message);
        }
        
        /// <summary>
        ///     proceed queue
        /// </summary>
        public List<IMessage> ProceedQueue()
        {
            var messages = queue.GetAllMessages();
            var proceedMsgs = new List<IMessage>();
            foreach (var message in messages)
            {
                var body = (IMessage) message.Body;
                proceedMsgs.Add(body);
            }

            queue.Purge();

            return proceedMsgs;
        }

        /// <summary>
        ///     get file name for log
        /// </summary>
        /// <returns>file name</returns>
        private static string GetFileNameForLog()
        {
            // set it to {folder}\{yyyy-MM-dd}.log 
            return Path.Combine(ConfigReader.LogFolderPath,
                string.Format("{0}.log", DateTime.Today.ToString("yyyy-MM-dd")));
        }

        /// <summary>
        ///     get log text
        /// </summary>
        /// <param name="result">result state</param>
        /// <param name="message">message</param>
        /// <returns></returns>
        private static string GetLogText(MessageProceedResult result, IMessage message)
        {
            if (result.Status == MessageProceedStatus.Success)
            {
                // set it to {messageId}\t{messageType}\t{outputFile}\t{status}
                return string.Format("{0}\t{1}\t{2}\t{3}" + Environment.NewLine,
                    message.MessageId.ToString(), message.MessageType.ToString(), result.FilePath,
                    result.Status.ToString());
            }
            else
            {
                // set it to {messageId}\t{messageType}\t{failedMessage}\t{status}
                return string.Format("{0}\t{1}\t{2}\t{3}" + Environment.NewLine,
                    message.MessageId.ToString(), message.MessageType.ToString(), result.FailureMessage,
                    result.Status.ToString());
            }
        }

        /// <summary>
        ///     log result
        /// </summary>
        /// <param name="result">result state</param>
        /// <param name="message">message</param>
        private static void LogResult(MessageProceedResult result, IMessage message)
        {
            File.AppendAllText(GetFileNameForLog(), GetLogText(result, message));
        }
    }
}